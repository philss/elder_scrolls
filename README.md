# Elder Scrolls

This is a simple sinatra app to work with sprockets.

## Installation

Inside the root path of this project, run:

```
$ bundle install
```

## Running

This command wil run the project on port 9292 (the default for rack apps):
```
$ bundle exec rackup
```
