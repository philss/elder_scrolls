require './app'

map('/') do
  run Sinatra::Application
end

map '/assets' do
  environment = Sprockets::Environment.new
  environment.append_path 'css'
  run environment
end
